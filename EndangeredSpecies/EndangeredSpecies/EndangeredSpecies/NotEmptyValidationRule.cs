﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Controls;

namespace EndangeredSpecies
{
    /// <summary>
    ///     Validation of ID fields in dialogs (Add/Edit Species)
    /// </summary>
    public class NotEmptyValidationRule : ValidationRule
    {
        /// <summary>
        ///     Method used for validation of ID fields in dialogs (Add/Edit Species)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return string.IsNullOrWhiteSpace((value ?? "").ToString())
                ? new ValidationResult(false, "Field is required.")
                : ValidationResult.ValidResult;
        }
    }
}
