﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for <c>Additional.xaml</c>
    /// </summary>
    /// <remarks>
    ///     <para>Class <c>Additional</c> represent additional options.
    ///     <list type="bullet">
    ///         <item><b>Help System</b> - helping user to understand the application.</item>
    ///         <item><b>Online Tutorial</b> - going with user through some of the tutorials that explain how to use application.</item>
    ///         <item><b>Online Documentation</b> - doucmentation of all the coding (don't believe I did that)</item>
    ///     </list>
    ///     </para>
    /// </remarks>
    public partial class Additional : UserControl
    {
        /// <summary>
        ///     Constructor that initializes <c>Additional</c> control component. 
        /// </summary>
        public Additional()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Handles button click on <b>Help</b> button.
        /// </summary>
        /// <remarks>
        ///     <para>Local variables:
        ///     <list type="bullet">
        ///         <item><code>curDir</code> is the current working directory path of the application.</item>
        ///         <item><code>key</code> is the name of the HTML file that opens when user wants to use Help System.</item>
        ///         <item><code>path</code> is two previous combined for finding absolute path to open in browser.</item>
        ///     </list>
        ///     Method <code>System.Diagnostics.Process.Start(fullPath)</code> is used to open local HTML in the default browser (Chrome, IE, etc.).
        ///     </para>
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Help_ButtonClick(object sender, RoutedEventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            string key = "index";
            string path = String.Format("{0}/HelpPages/{1}.html", curDir, key);
            if (!File.Exists(path))
            {
                key = "error";
            }
            string fullPath = String.Format("file:///{0}/HelpPages/{1}.html", curDir, key);

            System.Diagnostics.Process.Start(fullPath);
        }

        /// <summary>
        ///     Handles button click on <b>Online Tutorial</b> button.
        /// </summary>
        /// <remarks>
        ///     <para>Local variables:
        ///     <list type="bullet">
        ///         <item><code>curDir</code> is the current working directory path of the application.</item>
        ///         <item><code>key</code> is the name of the HTML file that opens when user wants to see Online Tutorial.</item>
        ///         <item><code>path</code> is two previous combined for finding absolute path to open in browser.</item>
        ///     </list>
        ///     Method <code>System.Diagnostics.Process.Start(fullPath)</code> is used to open local HTML in the default browser (Chrome, IE, etc.).
        ///     </para>
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnlineTutorial_ButtonClick(object sender, RoutedEventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            string key = "tutorial";
            string path = String.Format("{0}/OnlineTutorial/{1}.html", curDir, key);
            if (!File.Exists(path))
            {
                key = "tutorial";
            }
            curDir = "c:";
            string fullPath = String.Format("file:///{0}/OnlineTutorial/{1}.html", curDir, key);
            MessageBox.Show("put: " + String.Format("file:///{0}/OnlineTutorial/{1}.html", curDir, key));
            System.Diagnostics.Process.Start(fullPath);
        }

        /// <summary>
        ///     Handles button click on <b>Online DOcumentation</b> button.
        /// </summary>
        /// <remarks>
        ///     <para>Local variables:
        ///     <list type="bullet">
        ///         <item><code>curDir</code> is the current working directory path of the application.</item>
        ///         <item><code>key</code> is the name of the HTML file that opens when user wants to see Online Documentation.</item>
        ///         <item><code>path</code> is two previous combined for finding absolute path to open in browser.</item>
        ///     </list>
        ///     Method <code>System.Diagnostics.Process.Start(fullPath)</code> is used to open local HTML in the default browser (Chrome, IE, etc.).
        ///     </para>
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnlineDocumentation_ButtonClick(object sender, RoutedEventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            string key = "index";
            string path = String.Format("{0}/OnlineDocs/html/{1}.html", curDir, key);
            if (!File.Exists(path))
            {
                key = "index";  // Should be key="error", but this if is somehow true
            }
            curDir = "c:"; // This path changes
            MessageBox.Show("put: "+ String.Format("file:///{0}/OnlineDocs/html/{1}.html", curDir, key));
            string fullPath = String.Format("file:///{0}/OnlineDocs/html/{1}.html", curDir, key);

            System.Diagnostics.Process.Start(fullPath);
        }
    }
}
