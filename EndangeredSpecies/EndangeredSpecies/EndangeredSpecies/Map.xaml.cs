﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Collections.ObjectModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Logger;


using System.Diagnostics;
using EndangeredSpecies.Domain;
using MaterialDesignThemes.Wpf;
using System.Windows.Controls.Primitives;
using GongSolutions.Wpf.DragDrop;
using System.ComponentModel;
using System.Collections;

namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for Map.xaml
    ///     Map represents visual distribution of the Species around the globe.
    /// </summary>
    public partial class Map : UserControl, IDropTarget
    {
        /// <summary>
        ///     <c>Map</c> represents visual distribution of the Species around the globe.
        ///     User can drag and drop items from list to map and otherwise.
        ///     Usefull link and source reference: https://github.com/punker76/gong-wpf-dragdrop
        /// </summary>
        public Map()
        {
            DataLists dl = new DataLists();
            InitializeComponent();
            
            this.DataContext = this;

            Studenti = DataLists.speciesList;

            List<Species> l = new List<Species>();
            for (int i=0; i<70; i++)
            {
                l.Add(new Species());
            }
            Studenti2 = new ObservableCollection<Species>(l);

            prepareForTutorial();
        }

        /// <summary>
        ///     Prepare Map window in case user is in tutorial mode.
        /// </summary>
        public void prepareForTutorial()
        {
            if (DataLists.tutorialOn == true)
            {
                Canvas.SetZIndex(Tutorial, 12);
                Canvas.SetZIndex(FoggyRect, 10);

                Canvas.SetZIndex(MapDialog, 9);
                Canvas.SetZIndex(Tutorial0, 9);
                Canvas.SetZIndex(Tutorial1, 9);
                Canvas.SetZIndex(Tutorial2, 9);
                Canvas.SetZIndex(Tutorial3, 9);
                Canvas.SetZIndex(Tutorial4, 9);
                Canvas.SetZIndex(Tutorial5, 9);
                Canvas.SetZIndex(Tutorial6, 9);
                Canvas.SetZIndex(TutorialFinish, 9);
                //

                Canvas.SetZIndex(BackgroundRect0, 9);
                Canvas.SetZIndex(BackgroundRect1, 9);
                Canvas.SetZIndex(BackgroundRect2, 9);
                Canvas.SetZIndex(BackgroundRect3, 9);
                Canvas.SetZIndex(BackgroundRect4, 9);
                Canvas.SetZIndex(BackgroundRect5, 9);
                Canvas.SetZIndex(BackgroundRect6, 9);
                //

                FoggyRect.Visibility = Visibility.Visible;
                FoggyRect.Opacity = 0.5;

                BackgroundRect0.Visibility = Visibility.Hidden;
                BackgroundRect1.Visibility = Visibility.Hidden;
                BackgroundRect2.Visibility = Visibility.Hidden;
                BackgroundRect3.Visibility = Visibility.Hidden;
                BackgroundRect4.Visibility = Visibility.Hidden;
                BackgroundRect5.Visibility = Visibility.Hidden;
                BackgroundRect6.Visibility = Visibility.Hidden;
                //

                Tutorial.Visibility = Visibility.Visible;
                Tutorial0.Visibility = Visibility.Hidden;
                Tutorial1.Visibility = Visibility.Hidden;
                Tutorial2.Visibility = Visibility.Hidden;
                Tutorial3.Visibility = Visibility.Hidden;
                Tutorial4.Visibility = Visibility.Hidden;
                Tutorial5.Visibility = Visibility.Hidden;
                Tutorial6.Visibility = Visibility.Hidden;
                TutorialFinish.Visibility = Visibility.Hidden;
                //

            }
            else
            {
                Canvas.SetZIndex(Tutorial, 9);
                Canvas.SetZIndex(FoggyRect, 9);

                Canvas.SetZIndex(MapDialog, 12);
                Canvas.SetZIndex(Tutorial0, 9);
                Canvas.SetZIndex(Tutorial1, 9);
                Canvas.SetZIndex(Tutorial2, 9);
                Canvas.SetZIndex(Tutorial3, 9);
                Canvas.SetZIndex(Tutorial4, 9);
                Canvas.SetZIndex(Tutorial5, 9);
                Canvas.SetZIndex(Tutorial6, 9);
                Canvas.SetZIndex(TutorialFinish, 9);
                //

                Canvas.SetZIndex(BackgroundRect0, 9);
                Canvas.SetZIndex(BackgroundRect1, 9);
                Canvas.SetZIndex(BackgroundRect2, 9);
                Canvas.SetZIndex(BackgroundRect3, 9);
                Canvas.SetZIndex(BackgroundRect4, 9);
                Canvas.SetZIndex(BackgroundRect5, 9);
                Canvas.SetZIndex(BackgroundRect6, 9);
                //

                FoggyRect.Visibility = Visibility.Hidden;

                BackgroundRect0.Visibility = Visibility.Hidden;
                BackgroundRect1.Visibility = Visibility.Hidden;
                BackgroundRect2.Visibility = Visibility.Hidden;
                BackgroundRect3.Visibility = Visibility.Hidden;
                BackgroundRect4.Visibility = Visibility.Hidden;
                BackgroundRect5.Visibility = Visibility.Hidden;
                BackgroundRect6.Visibility = Visibility.Hidden;
                //

                Tutorial.Visibility = Visibility.Hidden;
                Tutorial0.Visibility = Visibility.Hidden;
                Tutorial1.Visibility = Visibility.Hidden;
                Tutorial2.Visibility = Visibility.Hidden;
                Tutorial3.Visibility = Visibility.Hidden;
                Tutorial4.Visibility = Visibility.Hidden;
                Tutorial5.Visibility = Visibility.Hidden;
                Tutorial6.Visibility = Visibility.Hidden;
                TutorialFinish.Visibility = Visibility.Hidden;
                //

            }
        }

        private void StartTutorial_ButtonClick(object sender, RoutedEventArgs e)
        {
            DataLists.tutorialOn = true;
            TutorialFileLogger.WriteInFile();

            Canvas.SetZIndex(Tutorial, 12);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Visible;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //

        }

        private void FinishTutorial_ButtonClick(object sender, RoutedEventArgs e)
        {
            DataLists.tutorialOn = false;
            TutorialFileLogger.WriteInFile();

            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 9);

            Canvas.SetZIndex(MapDialog, 12);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Hidden;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
        }

        private void StepTutorial0_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 12);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 11);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Visible;
            BackgroundRect0.Opacity = 0.5;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Visible;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
            step0to1 = false;
        }

        private void StepTutorial1_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 12);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 11);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Visible;
            BackgroundRect1.Opacity = 0.5;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Visible;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
            
        }

        private void StepTutorial2_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 12);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 11);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Visible;
            BackgroundRect2.Opacity = 0.5;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Visible;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
        }

        private void StepTutorial3_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 12);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 11);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Visible;
            BackgroundRect3.Opacity = 0.5;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Visible;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
        }

        private void StepTutorial4_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 12);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 11);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Visible;
            BackgroundRect4.Opacity = 0.5;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Visible;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
            step0to1 = true;
        }

        private void StepTutorial5_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 12);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 11);
            Canvas.SetZIndex(BackgroundRect6, 9);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Visible;
            BackgroundRect5.Opacity = 0.5;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Visible;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
            step0to1 = false;
        }

        private void StepTutorial6_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 12);
            Canvas.SetZIndex(TutorialFinish, 9);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 11);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Visible;
            BackgroundRect6.Opacity = 0.5;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Visible;
            TutorialFinish.Visibility = Visibility.Hidden;
            //
        }

        private void StepTutorialFinish_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(MapDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(Tutorial6, 9);
            Canvas.SetZIndex(TutorialFinish, 12);
            //

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            Canvas.SetZIndex(BackgroundRect6, 11);
            //

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            BackgroundRect6.Visibility = Visibility.Hidden;
            //

            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            Tutorial6.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Visible;
            //
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement focusedControl = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (focusedControl is DependencyObject)
            {
                string str = HelpProvider.GetHelpKey((DependencyObject)focusedControl);
                HelpProvider.ShowHelp(str, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void doThings(string param)
        {
            //btnOK.Background = new SolidColorBrush(Color.FromRgb(32, 64, 128));
            //Title = param;
        }

        Point startPoint = new Point();

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Species> Studenti
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Species> Studenti2
        {
            get;
            set;
        }

        private bool step0to1 = false;
        void IDropTarget.DragOver(IDropInfo dropInfo)
        {
            if (DataLists.tutorialOn == true && step0to1 == false)
            {
                // Step 1
                Canvas.SetZIndex(Tutorial, 9);
                Canvas.SetZIndex(FoggyRect, 10);

                Canvas.SetZIndex(MapDialog, 9);
                Canvas.SetZIndex(Tutorial0, 9);
                Canvas.SetZIndex(Tutorial1, 12);
                Canvas.SetZIndex(Tutorial2, 9);
                Canvas.SetZIndex(Tutorial3, 9);
                Canvas.SetZIndex(Tutorial4, 9);
                Canvas.SetZIndex(Tutorial5, 9);
                Canvas.SetZIndex(Tutorial6, 9);
                Canvas.SetZIndex(TutorialFinish, 9);
                //

                Canvas.SetZIndex(BackgroundRect0, 9);
                Canvas.SetZIndex(BackgroundRect1, 11);
                Canvas.SetZIndex(BackgroundRect2, 9);
                Canvas.SetZIndex(BackgroundRect3, 9);
                Canvas.SetZIndex(BackgroundRect4, 9);
                Canvas.SetZIndex(BackgroundRect5, 9);
                Canvas.SetZIndex(BackgroundRect6, 9);
                //

                FoggyRect.Visibility = Visibility.Visible;
                FoggyRect.Opacity = 0.5;

                BackgroundRect0.Visibility = Visibility.Hidden;
                BackgroundRect1.Visibility = Visibility.Visible;
                BackgroundRect1.Opacity = 0.5;
                BackgroundRect2.Visibility = Visibility.Hidden;
                BackgroundRect3.Visibility = Visibility.Hidden;
                BackgroundRect4.Visibility = Visibility.Hidden;
                BackgroundRect5.Visibility = Visibility.Hidden;
                BackgroundRect6.Visibility = Visibility.Hidden;
                //

                Tutorial.Visibility = Visibility.Hidden;
                Tutorial0.Visibility = Visibility.Hidden;
                Tutorial1.Visibility = Visibility.Visible;
                Tutorial2.Visibility = Visibility.Hidden;
                Tutorial3.Visibility = Visibility.Hidden;
                Tutorial4.Visibility = Visibility.Hidden;
                Tutorial5.Visibility = Visibility.Hidden;
                Tutorial6.Visibility = Visibility.Hidden;
                TutorialFinish.Visibility = Visibility.Hidden;

                
            } else
            {
                if (DataLists.tutorialOn == true && step0to1 == true)
                {
                    // Step 5
                    Canvas.SetZIndex(Tutorial, 9);
                    Canvas.SetZIndex(FoggyRect, 10);

                    Canvas.SetZIndex(MapDialog, 9);
                    Canvas.SetZIndex(Tutorial0, 9);
                    Canvas.SetZIndex(Tutorial1, 9);
                    Canvas.SetZIndex(Tutorial2, 9);
                    Canvas.SetZIndex(Tutorial3, 9);
                    Canvas.SetZIndex(Tutorial4, 9);
                    Canvas.SetZIndex(Tutorial5, 12);
                    Canvas.SetZIndex(Tutorial6, 9);
                    Canvas.SetZIndex(TutorialFinish, 9);
                    //

                    Canvas.SetZIndex(BackgroundRect0, 9);
                    Canvas.SetZIndex(BackgroundRect1, 9);
                    Canvas.SetZIndex(BackgroundRect2, 9);
                    Canvas.SetZIndex(BackgroundRect3, 9);
                    Canvas.SetZIndex(BackgroundRect4, 9);
                    Canvas.SetZIndex(BackgroundRect5, 11);
                    Canvas.SetZIndex(BackgroundRect6, 9);
                    //

                    FoggyRect.Visibility = Visibility.Visible;
                    FoggyRect.Opacity = 0.5;

                    BackgroundRect0.Visibility = Visibility.Hidden;
                    BackgroundRect1.Visibility = Visibility.Hidden;
                    BackgroundRect2.Visibility = Visibility.Hidden;
                    BackgroundRect3.Visibility = Visibility.Hidden;
                    BackgroundRect4.Visibility = Visibility.Hidden;
                    BackgroundRect5.Visibility = Visibility.Visible;
                    BackgroundRect5.Opacity = 0.5;
                    BackgroundRect6.Visibility = Visibility.Hidden;
                    //

                    Tutorial.Visibility = Visibility.Hidden;
                    Tutorial0.Visibility = Visibility.Hidden;
                    Tutorial1.Visibility = Visibility.Hidden;
                    Tutorial2.Visibility = Visibility.Hidden;
                    Tutorial3.Visibility = Visibility.Hidden;
                    Tutorial4.Visibility = Visibility.Hidden;
                    Tutorial5.Visibility = Visibility.Visible;
                    Tutorial6.Visibility = Visibility.Hidden;
                    TutorialFinish.Visibility = Visibility.Hidden;

                    
                }
            }

            Species sourceItem = dropInfo.Data as Species;
            Species targetItem = dropInfo.TargetItem as Species;
            int border = 70;

            ICollection<Species> targetCol = dropInfo.TargetCollection as ICollection<Species>;
            ICollection<Species> sourceCol = dropInfo.DragInfo.SourceCollection as ICollection<Species>;

            if (sourceItem != null && targetItem != null)
            {
                if (targetCol.Count == border) // <-
                {
                    dropInfo.DropTargetAdorner = DropTargetAdorners.Highlight;
                    dropInfo.Effects = DragDropEffects.Move;
                    dropInfo.DestinationText = "Move to";
                } else {
                    dropInfo.Effects = DragDropEffects.Move;
                    dropInfo.DestinationText = "Move back";
                }
            }
        }

        void IDropTarget.Drop(IDropInfo dropInfo)
        {
            Species sourceItem = dropInfo.Data as Species;
            Species targetItem = dropInfo.TargetItem as Species;
            int border = 70;

            ICollection<Species> targetCol = dropInfo.TargetCollection as ICollection<Species>;
            ICollection<Species> sourceCol = dropInfo.DragInfo.SourceCollection as ICollection<Species>;

            if (sourceCol.Count != border && targetCol.Count == border) // <-
            {
                Studenti2.RemoveAt(dropInfo.InsertIndex);
                Studenti2.Insert(dropInfo.InsertIndex, sourceItem);
                Studenti.Remove(sourceItem);
                
            }
            if (sourceCol.Count == border && targetCol.Count == border) // l
            {
                Studenti2.Remove(sourceItem);
                Studenti2.Insert(dropInfo.InsertIndex, sourceItem);

            }
            if (sourceCol.Count == border && targetCol.Count != border) // ->
            {
                if (sourceItem.Id != null) { 
                    Studenti2.Remove(sourceItem);
                    Studenti2.Add(new Species());
                    Studenti.Add(sourceItem);
                }
            }

            //Studenti.Remove(sourceItem);
            //Studenti2.Remove(targetItem);
        }



        private void ListView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private async void listViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;
            object obj = item.Content;
            //MessageBox.Show(((Species)obj).Id + " is id");

            if (((Species)obj) != null) {
                if (((Species)obj).Id != null)
                {
                    if (!((Species)obj).Id.Equals(""))
                    {

                        Species speciesData = DataLists.FindSpeciesById(((Species)obj).Id);

                        string imgPath = (string)speciesData.Image;
                        if (imgPath.Equals(""))
                        {
                            imgPath = speciesData.Type.Image;
                        }

                        var sampleMessageDialog = new SpeciesDetailsDialog
                        {
                            SpeciesId = { Text = (speciesData.Id) },
                            SpeciesName = { Text = (speciesData.Name) },
                            SpeciesDate = { SelectedDate = (speciesData.DiscoveryDate) },
                            SpeciesType = { SelectedItem = (speciesData.Type), SelectedValue = (speciesData.Type.Name) },
                            SpeciesEndangeredStatus = { SelectedItem = (speciesData.EndangeredStatus) },
                            SpeciesTouristStatus = { SelectedItem = (speciesData.TouristStatus) },
                            SpeciesAnnualIncome = { Text = (speciesData.AnnualTourismIncome.ToString()) },
                            SpeciesDescription = { Text = (speciesData.Description) },
                            SpeciesImage = { Source = new BitmapImage(new Uri(imgPath, imgPath.Substring(0, 2).Equals("C:") ? UriKind.Absolute : UriKind.Relative)) },
                            //SpeciesTags = { Text = (speciesData.Tags) },
                            SpeciesIsDangerousYES = { IsChecked = (speciesData.IsDangerous == true ? true : false) },
                            SpeciesIsDangerousNO = { IsChecked = (speciesData.IsDangerous == true ? false : true) },
                            SpeciesIsOnIUCNYES = { IsChecked = (speciesData.IsOnIUCNRedList == true ? true : false) },
                            SpeciesIsOnIUCNNO = { IsChecked = (speciesData.IsOnIUCNRedList == true ? false : true) },
                            SpeciesLivesInYES = { IsChecked = (speciesData.LivesInPopulatedRegion == true ? true : false) },
                            SpeciesLivesInNO = { IsChecked = (speciesData.LivesInPopulatedRegion == true ? false : true) },

                        };

                        SpeciesDialogViewModel.SpeciesTags = speciesData.TagsOfSpecies;

                        await DialogHost.Show(sampleMessageDialog, "RootDialog");
                    }
                }
            }
        }

        /*private void ListView_MouseMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                // Get the dragged ListViewItem
                ListView listView = sender as ListView;
                ListViewItem listViewItem =
                    FindAncestor<ListViewItem>((DependencyObject)e.OriginalSource);

                // Find the data behind the ListViewItem
                Species student = (Species)listView.ItemContainerGenerator.
                    ItemFromContainer(listViewItem);

                // Initialize the drag & drop operation
                DataObject dragData = new DataObject("myFormat", student);
                DragDrop.DoDragDrop(listViewItem, dragData, DragDropEffects.Move);
            }
        }*/

        private double slidebarValue = 0.2;
        private void OpacitySlidebar_ValueChanged(object sender, RoutedEventArgs e)
        {
            
            for (int i=0; i < DataLists.speciesList.Count; i++)
            {
                if (OpacitySlidebar != null)
                    DataLists.speciesList[i].Opacity = (OpacitySlidebar.Value / 100).ToString();

            }
            if (Studenti != null)
                for (int i = 0; i < Studenti.Count; i++)
                {
                    if (Studenti[i].Id != null)
                        Studenti[i].Opacity = (OpacitySlidebar.Value / 100).ToString();
                }
        }

        private void OpacitySlidebar2_ValueChanged(object sender, RoutedEventArgs e)
        {
            if (OpacitySlidebar != null)
                OpacitySlidebar.Value = OpacitySlidebar2.Value;
           
            for (int i = 0; i < DataLists.speciesList.Count; i++)
            {
                
                if (OpacitySlidebar2 != null)
                {
                    DataLists.speciesList[i].Opacity = (OpacitySlidebar2.Value / 100).ToString();
                }
            }
            if (Studenti != null)
                for (int i = 0; i < Studenti.Count; i++)
                {
                    if (Studenti[i].Id != null)
                        Studenti[i].Opacity = (OpacitySlidebar2.Value / 100).ToString();
                }
        }

        private static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        private void ListView_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("myFormat") || sender == e.Source)
            {
                e.Effects = DragDropEffects.Copy;
            } else
            {
                // TODO Not sure :/
                e.Effects = DragDropEffects.Copy;
            }
        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("myFormat"))
            {
                Species student = e.Data.GetData("myFormat") as Species;
                Studenti.Remove(student);
                Studenti2.Add(student);
            }
        }

        
    }
}
