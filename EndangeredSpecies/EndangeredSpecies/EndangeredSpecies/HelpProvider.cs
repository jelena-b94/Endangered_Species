﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using EndangeredSpecies.Domain;

namespace EndangeredSpecies
{
    /// <summary>
    ///     <c>Help Provider is used for Help system.</c>
    /// </summary>
    public class HelpProvider
    {
        /// <summary>
        ///     Gets the string key. Keys are the names of components for which is needed help.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetHelpKey(DependencyObject obj)
        {
            return obj.GetValue(HelpKeyProperty) as string;
        }

        /// <summary>
        ///     Sets the string key. Keys are the names of components for which is needed help.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void SetHelpKey(DependencyObject obj, string value)
        {
            obj.SetValue(HelpKeyProperty, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty HelpKeyProperty =
            DependencyProperty.RegisterAttached("HelpKey", typeof(string), typeof(HelpProvider), new PropertyMetadata("MainWindow", HelpKey)); // Can be index instead of MainWindow.
        private static void HelpKey(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // NOOP
        }

        /// <summary>
        ///     Method that is called to open help window for selected component.
        ///     In this case help is needed on <c>MainWindow</c>.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public static void ShowHelp(string key, MainWindow originator)
        {
            HelpViewer hh = new HelpViewer(key, originator);
            hh.Show();
        }

        /// <summary>
        ///      Method that is called to open help window for selected component.
        ///      In this case help is needed on <c>Home</c> window.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public static void ShowHelp(string key, Home originator)
        {
            HelpViewer hh = new HelpViewer(key, originator);
            hh.Show();
        }

        /// <summary>
        ///     Method that is called to open help window for selected component.
        ///     In this case help is needed on <c>Tables</c> window.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public static void ShowHelp(string key, Tables originator)
        {
            HelpViewer hh = new HelpViewer(key, originator);
            hh.Show();
        }

        /// <summary>
        ///     Method that is called to open help window for selected component.
        ///     In this case help is needed on <c>Map</c> window.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public static void ShowHelp(string key, Map originator)
        {
            HelpViewer hh = new HelpViewer(key, originator);
            hh.Show();
        }

        /// <summary>
        ///     Method that is called to open help window for selected component.
        ///     In this case help is needed on <c>Species Dialog</c> window.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public static void ShowHelp(string key, SpeciesDialog originator)
        {
            HelpViewer hh = new HelpViewer(key, originator);
            hh.Show();
        }
    }
}
