﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using EndangeredSpecies.Logger;
using System.Collections.ObjectModel;
using EndangeredSpecies.Classes;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     Interaction logic for SpeciesDetailsDialog.xaml
    /// </summary>
    public partial class SpeciesDetailsDialog : UserControl
    {
        /// <summary>
        ///     Default constructor
        /// </summary>
        public SpeciesDetailsDialog()
        {
            DataLists dl = new DataLists();
            InitializeComponent();
            DataContext = new SpeciesDialogViewModel();
           
        }

        
    }
}
