﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Domain;
using EndangeredSpecies.Logger;
using MaterialDesignThemes.Wpf;




namespace EndangeredSpecies.Domain
{
    /// <summary>
    /// Interaction logic for SpeciesDialog.xaml
    /// </summary>
    public partial class SpeciesDialog : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public bool yesSelected = false;

        /// <summary>
        /// 
        /// </summary>
        public Species newSpecies = null;
        private List<Tag> selectedTags = new List<Tag>();
        private List<Tag> selectedSpeciesTags = new List<Tag>();

        /// <summary>
        /// 
        /// </summary>
        public SpeciesDialog()
        {
            DataLists dl = new DataLists();
            InitializeComponent();
            DataContext = new SpeciesDialogViewModel();
            SaveButton.IsEnabled = false;
            
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement focusedControl = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (focusedControl is DependencyObject)
            {
                string str = HelpProvider.GetHelpKey((DependencyObject)focusedControl);
                HelpProvider.ShowHelp(str, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void doThings(string param)
        {
            //btnOK.Background = new SolidColorBrush(Color.FromRgb(32, 64, 128));
            //Title = param;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void CalendarDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {

            SpeciesDate.SelectedDate = SpeciesDialogViewModel.Date;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void CalendarDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (!Equals(eventArgs.Parameter, "1")) return;

            if (!SpeciesDate.SelectedDate.HasValue)
            {
                eventArgs.Cancel();
                return;
            }

            SpeciesDialogViewModel.Date = SpeciesDate.SelectedDate.Value;
            SpeciesShowDate.Text = SpeciesDialogViewModel.Date.ToString("d");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void DeleteSelectedTags_ButtonClicked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < selectedSpeciesTags.Count; i++)
            {
                SpeciesDialogViewModel.Tags.Add(selectedSpeciesTags[i]);

            }

            for (int i = 0; i < selectedSpeciesTags.Count; i++)
            {
                int j = 0;
                while (j < SpeciesDialogViewModel.SpeciesTags.Count)
                {
                    if (selectedSpeciesTags[i].Id.Equals(SpeciesDialogViewModel.SpeciesTags[j].Id))
                    {
                        SpeciesDialogViewModel.SpeciesTags.Remove(SpeciesDialogViewModel.SpeciesTags[j]);

                    }
                    else
                    {
                        j++;
                    }
                }
            }


            selectedSpeciesTags.Clear();
        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            
            
            if (!Equals(eventArgs.Parameter, true))
            {
                selectedTags.Clear();
                return;
            }
            foreach (var tag in selectedTags)
            {
                SpeciesDialogViewModel.SpeciesTags.Add(tag);
            }
            foreach (var tag in SpeciesDialogViewModel.SpeciesTags)
            {              
                SpeciesDialogViewModel.Tags.Remove(tag);
            }
            selectedTags.Clear();

        }



        private void listView_Tags_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (Tag item in e.RemovedItems)
            {
                selectedTags.Remove(item);
            }

            foreach (Tag item in e.AddedItems)
            {
                selectedTags.Add(item);
            }
        }

        private void listView_SpeciesTags_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (Tag item in e.RemovedItems)
            {
                selectedSpeciesTags.Remove(item);
            }

            foreach (Tag item in e.AddedItems)
            {
                selectedSpeciesTags.Add(item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ImgPath = "";
        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png";
            ofd.Multiselect = false;

            Nullable<bool> ok = ofd.ShowDialog();
            if (ok == true)
            {
                ImgPath = ofd.FileName;
                BitmapImage image = new BitmapImage(new Uri(ImgPath, UriKind.Absolute));
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.Freeze();
                SpeciesImage.Source = image;

            }
        }

        private void Field_Changed(object sender, RoutedEventArgs e)
        {
            if (!SpeciesId.Text.Equals(""))
            {
                double annualIncome = 0;
                if (!SpeciesAnnualIncome.Text.ToString().Equals(""))
                {
                    if (double.TryParse(SpeciesAnnualIncome.Text.ToString(), out annualIncome))
                    {
                        SaveButton.IsEnabled = true;
                    }
                    else
                    {
                        SaveButton.IsEnabled = false;
                    }
                }
                else
                {
                    SaveButton.IsEnabled = true;
                }
            }
            else
            {
                SaveButton.IsEnabled = false;
            }
        }

        private async void SearchButton_OnClick(object sender, RoutedEventArgs e)
        {
            SpeciesDialogViewModel.RefreshTags();

            int i = 0;
            while (i < SpeciesDialogViewModel.Tags.Count)
            {
                //bool b = SpeciesDialogViewModel.Tags[i].Color.IndexOf(TextToSearch.Text.ToString(), StringComparison.OrdinalIgnoreCase)>=0;
                if (SpeciesDialogViewModel.Tags[i].Color.IndexOf(TextToSearch.Text.ToString(), StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    i++;
                }
                else
                {
                    SpeciesDialogViewModel.Tags.RemoveAt(i);
                }
            }
        }
        

        private async void DisposeSearchButton_OnClick(object sender, RoutedEventArgs e)
        {
            TextToSearch.Text = "";

            SpeciesDialogViewModel.RefreshTags();
        }

        private async void SelectedYESCommand(object sender, RoutedEventArgs e)
        {
            yesSelected = true;

            newSpecies = new Species();           
            newSpecies.Id = SpeciesId.Text.ToString();
            newSpecies.Name = SpeciesName.Text.ToString();
            //newSpecies.DiscoveryDate = (DateTime)SpeciesDate.SelectedDate;
            if (SpeciesDate.SelectedDate.HasValue)
            {
                newSpecies.DiscoveryDate = (DateTime)SpeciesDate.SelectedDate;
            }
            else
            {
                DateTime NullDate = DateTime.MinValue;
                //DateTime dateOfBirth = NullDate;
                newSpecies.DiscoveryDate = NullDate;
            }

            if (((SpeciesType)SpeciesType.SelectedItem) == null)
                newSpecies.Type = null;
            else
                newSpecies.Type = DataLists.FindTypeByName(((SpeciesType)SpeciesType.SelectedItem).Name.ToString());

            newSpecies.EndangeredStatus = SpeciesEndangeredStatus.Text.ToString();
            newSpecies.TouristStatus = SpeciesTouristStatus.Text.ToString();

            double annualIncome = 0;
            if (!double.TryParse(SpeciesAnnualIncome.Text.ToString(), out annualIncome))
                newSpecies.AnnualTourismIncome = 0;
            newSpecies.AnnualTourismIncome = annualIncome;

            newSpecies.Description = SpeciesDescription.Text.ToString();
            if (ImgPath.Equals(""))
            {
                ImgPath = newSpecies.Type.Image;
            }
            newSpecies.Image = ImgPath;
            newSpecies.TagsOfSpecies = SpeciesDialogViewModel.SpeciesTags;
            newSpecies.IsDangerous = (bool)SpeciesIsDangerousYES.IsChecked;
            newSpecies.IsOnIUCNRedList = (bool)SpeciesIsOnIUCNYES.IsChecked;
            newSpecies.LivesInPopulatedRegion = (bool)SpeciesLivesInYES.IsChecked;
            

        }
    }
}