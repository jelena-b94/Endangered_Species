﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using EndangeredSpecies.Classes;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     For appearance of the species in table of species
    /// </summary>
    public class SelectableViewModel : INotifyPropertyChanged
    {
        private string _id;
        private string _name;
        private string _type;
        private string _endangeredStatus;
        private string _dangerous;
        private string _onIUCNredList;

        /// <summary>
        ///     Method that converts species to selectable species (adaptation).
        /// </summary>
        /// <param name="species"></param>
        /// <returns></returns>
        public SelectableViewModel convertSpeciesToTableItem(Species species)
        {
            SelectableViewModel svm = new SelectableViewModel();
            svm.ID = species.Id.ToString();
            svm.Name = species.Name.ToString();
            svm.Type = species.Type.Name.ToString();
            svm.EndangeredStatus = species.EndangeredStatus.ToString();
            svm.Dangerous = species.IsDangerous == true? "Yes":"No";
            svm.OnIUCNRedList = species.IsOnIUCNRedList == true ? "Yes" : "No";
            return svm;
        }

        

        /// <summary>
        ///     Gets or sets species ID inside the table
        /// </summary>
        public string ID
        {
            get { return _id; }
            set
            {
                if (_id == value) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets endangered status of the species inside the table
        /// </summary>
        public string EndangeredStatus
        {
            get { return _endangeredStatus; }
            set
            {
                if (_endangeredStatus == value) return;
                _endangeredStatus = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets species name inside the table
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name == value) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets species type inside the table
        /// </summary>
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets danger status of the species inside the table
        /// </summary>
        public string Dangerous
        {
            get { return _dangerous; }
            set
            {
                if (_dangerous == value) return;
                _dangerous = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets value if the species is on the IUCN Red List of threatened species
        /// </summary>
        public string OnIUCNRedList
        {
            get { return _onIUCNredList; }
            set
            {
                if (_onIUCNredList == value) return;
                _onIUCNredList = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Event handler for property changed (clicked on the item of the table)
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Method what to do when clicked on the item of the table
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
