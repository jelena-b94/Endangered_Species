﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EndangeredSpecies.Classes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     Interaction logic for SpeciesDeleteDialog.xaml
    /// </summary>
    public partial class SpeciesDeleteDialog : UserControl
    {
        /// <summary>
        ///     Before closing, user decided to delete permanently
        /// </summary>
        public bool yesSelected = false;

        /// <summary>
        ///     Default constructor
        /// </summary>
        public SpeciesDeleteDialog()
        {
            InitializeComponent();
            
        }

        /// <summary>
        ///     TableViewModel is connected with DataContext
        /// </summary>
        public TableViewModel ViewModel => DataContext as TableViewModel;

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SelectedYESCommand(object sender, RoutedEventArgs e)
        {
            yesSelected = true;
            
        }

        
    }
}
