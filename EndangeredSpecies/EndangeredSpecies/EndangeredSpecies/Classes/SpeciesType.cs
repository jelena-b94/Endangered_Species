﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace EndangeredSpecies.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class SpeciesType
    {
        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<SpeciesType> speciesTypes;

        /// <summary>
        /// 
        /// </summary>
        public SpeciesType()
        {
            Id = "";
            Name = "";
            Description = "";
            Image = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="image"></param>
        public SpeciesType(string id, string name, string description, string image)
        {
            Id = id;
            Name = name;
            Description = description;
            Image = image;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Image { get; set; }

        
    }
}
