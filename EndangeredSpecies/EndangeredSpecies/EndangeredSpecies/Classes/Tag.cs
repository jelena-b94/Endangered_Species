﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace EndangeredSpecies.Classes
{
    /// <summary>
    ///     <c>Tag</c> is used for marking the species (mostly becouse of colors).
    /// </summary>
    public sealed class Tag
    {
        /// <summary>
        ///     Empty constructor
        /// </summary>
        public Tag()
        {
            Id = "";
            Color = "";
            Description = "";
        }

        /// <summary>
        ///     Default constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="color"></param>
        /// <param name="description"></param>
        public Tag(string id, string color, string description)
        {
            Id = id;
            Color = color;
            Description = description;
        }

        /// <summary>
        ///     Gets or sets tag ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets tag color
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        ///     Gets or sets tag desciption
        /// </summary>
        public string Description { get; set; }

    }
}
