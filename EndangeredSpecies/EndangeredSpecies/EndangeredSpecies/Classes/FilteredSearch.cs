﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace EndangeredSpecies.Classes
{
    /// <summary>
    ///     <c>FilteredSearch</c> is class whose objects are created when user uses filter during search.
    /// </summary>
    public sealed class FilteredSearch
    {
        /// <summary>
        ///     Types of endangered statuses of species.
        /// </summary>
        public static ObservableCollection<string> EndageredStatuses = new ObservableCollection<string>(new string[] { "Critically endangered", "Endangered", "Vulnerable",
            "Dependent on habitats preservation", "Close risk", "Least risk", "All" });

        /// <summary>
        ///     Types of tourist statuses of species
        /// </summary>
        public static ObservableCollection<string> TouristStatuses = new ObservableCollection<string>(new string[] { "Isolated", "Partially Habitated", "Habitated", "All" });

        /// <summary>
        ///     Is species dangerous?
        /// </summary>
        public static ObservableCollection<string> IsDangerous = new ObservableCollection<string>(new string[] { "Yes", "No", "Other" });

        /// <summary>
        ///     Is species on IUCN Red List of threatened species?
        /// </summary>
        public static ObservableCollection<string> IsOnIUCNRedList = new ObservableCollection<string>(new string[] { "Yes", "No", "Other" });

        /// <summary>
        ///     Does species live in populated region?
        /// </summary>
        public static ObservableCollection<string> LivesInPopulatedRegion = new ObservableCollection<string>(new string[] { "Yes", "No", "Other" });

        /// <summary>
        ///     Empty constructor
        /// </summary>
        public FilteredSearch() { }

        /// <summary>
        ///     Main constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="endangeredStatus"></param>
        /// <param name="isDangerous"></param>
        /// <param name="isOnIUCNRedList"></param>
        /// <param name="livesInPopulatedRegion"></param>
        /// <param name="touristStatus"></param>
        /// <param name="discoveryDateFROM"></param>
        /// <param name="discoveryDateTO"></param>
        /// <param name="annualTourismIncomeFROM"></param>
        /// <param name="annualTourismIncomeTO"></param>
        public FilteredSearch(SpeciesType type, string endangeredStatus, string isDangerous,
            string isOnIUCNRedList, string livesInPopulatedRegion, string touristStatus, DateTime discoveryDateFROM, DateTime discoveryDateTO, 
            double annualTourismIncomeFROM, double annualTourismIncomeTO)
        {           
            Type = type;
            EndangeredStatus = endangeredStatus;
            IsDangerousChoice = isDangerous;
            IsOnIUCNRedListChoice = isOnIUCNRedList;
            LivesInPopulatedRegionChoice = livesInPopulatedRegion;
            TouristStatus = touristStatus;
            DiscoveryDateFROM = discoveryDateFROM;
            DiscoveryDateTO = discoveryDateTO;
            AnnualTourismIncomeFROM = annualTourismIncomeFROM;
            AnnualTourismIncomeTO = annualTourismIncomeTO;
        }

        /// <summary>
        ///     Gets or sets type of the species user wants to find
        /// </summary>
        public SpeciesType Type { get; set; }

        /// <summary>
        ///     Gets or sets endangered status of species user wants to find
        /// </summary>
        public string EndangeredStatus { get; set; }

        /// <summary>
        ///     Gets or sets the danger status of species user wants to find
        /// </summary>
        public string IsDangerousChoice { get; set; }

        /// <summary>
        ///     Gets or sets is the species on IUCN Red List of threatened species user wants to find
        /// </summary>
        public string IsOnIUCNRedListChoice { get; set; }

        /// <summary>
        ///     Gets or sets value of does species lives in populated region user wants to find
        /// </summary>
        public string LivesInPopulatedRegionChoice { get; set; }

        /// <summary>
        ///     Gets or sets tourist status of species user wants to find
        /// </summary>
        public string TouristStatus { get; set; }

        /// <summary>
        ///     Gets or sets first parameter (FROM) of discovery date of species range user wants to find
        /// </summary>
        public DateTime DiscoveryDateFROM { get; set; }

        /// <summary>
        ///     Gets or sets last parameter (TO) of discovery date of species range user wants to find
        /// </summary>
        public DateTime DiscoveryDateTO { get; set; }

        /// <summary>
        ///     Gets or sets first parameter (FROM) of annual tourism income for species user wants to find
        /// </summary>
        public double AnnualTourismIncomeFROM { get; set; }

        /// <summary>
        ///     Gets or sets last parameter (TO) of annual tourism income for species user wants to find
        /// </summary>
        public double AnnualTourismIncomeTO { get; set; }

    }
}
