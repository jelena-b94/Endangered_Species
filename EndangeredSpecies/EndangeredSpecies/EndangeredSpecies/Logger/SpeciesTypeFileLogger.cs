﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EndangeredSpecies.Classes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace EndangeredSpecies.Logger
{
    /// <summary>
    ///     SpeciesTypeFileLogger comunicates with text files that contain all the species types for system
    /// </summary>
    public sealed class SpeciesTypeFileLogger
    {
        /// <summary>
        ///    Write inside the file of species types 
        /// </summary>
        public static void WriteInFile()
        {

            string[] lines = new string[DataLists.speciesTypeList.Count];
            for (int i = 0; i < DataLists.speciesTypeList.Count; i++)
            {
                String line = DataLists.speciesTypeList[i].Id + "|" + DataLists.speciesTypeList[i].Name + "|" +
                    DataLists.speciesTypeList[i].Description + "|" +  DataLists.speciesTypeList[i].Image;
                lines[i] = line;
            }

            System.IO.File.WriteAllLines("SpeciesType.txt", lines);
        }

        /// <summary>
        ///     Reads all the species from files that contain species types
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<SpeciesType> ReadFile()
        {
            ObservableCollection<SpeciesType> list = new ObservableCollection<SpeciesType>();
            try
            {
                if (!System.IO.File.Exists("SpeciesType.txt"))
                    throw new System.IO.FileNotFoundException();
                string[] lines = System.IO.File.ReadAllLines("SpeciesType.txt");
                
                foreach (string line in lines)
                {
                    string[] words = line.Split('|');

                    SpeciesType species = new SpeciesType();
                    species.Id = words[0];
                    species.Name = words[1];
                    species.Description = words[2];
                    species.Image = words[3];
                    

                    list.Add(species);

                }
                return list;
            }
            catch (System.IO.FileNotFoundException e)
            {
                // your message here.
            }
            list = DataLists.CollectDataSpeciesType();
            return list;
        }
    }
}
