﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EndangeredSpecies.Classes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace EndangeredSpecies.Logger
{
    /// <summary>
    ///     TagFileLogger comunicates with text files that contain all the species for system
    /// </summary>
    public sealed class TagFileLogger
    {
        /// <summary>
        ///     Write inside the file of tags
        /// </summary>
        public static void WriteInFile()
        {

            string[] lines = new string[DataLists.tagList.Count];
            for (int i = 0; i < DataLists.tagList.Count; i++)
            {
                String line = DataLists.tagList[i].Id + "|" + DataLists.tagList[i].Color + "|" +
                    DataLists.tagList[i].Description;
                lines[i] = line;
            }

            System.IO.File.WriteAllLines("Tag.txt", lines);
        }

        /// <summary>
        ///     Reads all the tags from files that contain tags
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<Tag> ReadFile()
        {
            ObservableCollection<Tag> list = new ObservableCollection<Tag>();
            try
            {
                if (!System.IO.File.Exists("Tag.txt"))
                    throw new System.IO.FileNotFoundException();
                string[] lines = System.IO.File.ReadAllLines("Tag.txt");
                
                foreach (string line in lines)
                {
                    string[] words = line.Split('|');

                    Tag species = new Tag();
                    species.Id = words[0];
                    species.Color = words[1];
                    species.Description = words[2];


                    list.Add(species);

                }
                return list;
            }
            catch (System.IO.FileNotFoundException e)
            {
                // your message here.
            }
            list = DataLists.CollectDataTag();
            return list;
        }
    }
}
