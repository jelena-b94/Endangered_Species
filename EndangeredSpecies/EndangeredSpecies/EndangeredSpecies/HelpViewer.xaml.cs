﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using EndangeredSpecies.Domain;

namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for <c>HelpViewer.xaml</c>
    ///     Opens small window (HelpViewer) that provides help to user.
    /// </summary>
    public partial class HelpViewer : Window
    {   
        private JavaScriptControlHelper ch;

        /// <summary>
        ///     This HelpViewer is opened by <c>MainWindow</c> calling.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public HelpViewer(string key, MainWindow originator)
        {
            InitializeComponent();
            string curDir = Directory.GetCurrentDirectory();
            string path = String.Format("{0}/HelpPages/{1}.html", curDir, key); MessageBox.Show("Key " + key);
            if (!File.Exists(path))
            {
                key = "MainWindow"; // Should be error
            }
            Uri u = new Uri(String.Format("file:///{0}/HelpPages/{1}.html", curDir, key));
            MessageBox.Show("Uri " + u);
            ch = new JavaScriptControlHelper(originator);
            wbHelp.ObjectForScripting = ch;
            wbHelp.Navigate(u);

        }

        /// <summary>
        ///     This HelpViewer is opened by <c>Home</c> window calling.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public HelpViewer(string key, Home originator)
        {
            InitializeComponent();
            string curDir = Directory.GetCurrentDirectory();
            string path = String.Format("{0}/HelpPages/{1}.html", curDir, key); MessageBox.Show("Key " + key);
            if (!File.Exists(path))
            {
                key = "Home";  // Should be error
            }
            Uri u = new Uri(String.Format("file:///{0}/HelpPages/{1}.html", curDir, key));
            MessageBox.Show("Uri " + u);
            ch = new JavaScriptControlHelper(originator);
            wbHelp.ObjectForScripting = ch;
            wbHelp.Navigate(u);

        }

        /// <summary>
        ///     This HelpViewer is opened by <c>Tables</c> window calling.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public HelpViewer(string key, Tables originator)
        {
            InitializeComponent();
            string curDir = Directory.GetCurrentDirectory();
            string path = String.Format("{0}/HelpPages/{1}.html", curDir, key); MessageBox.Show("Key " + key);
            if (!File.Exists(path))
            {
                key = "Tables"; // Should be error
            }
            Uri u = new Uri(String.Format("file:///{0}/HelpPages/{1}.html", curDir, key));
            MessageBox.Show("Uri " + u);
            ch = new JavaScriptControlHelper(originator);
            wbHelp.ObjectForScripting = ch;
            wbHelp.Navigate(u);

        }

        /// <summary>
        ///     This HelpViewer is opened by <c>Map</c> window calling.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public HelpViewer(string key, Map originator)
        {
            InitializeComponent();
            string curDir = Directory.GetCurrentDirectory();
            string path = String.Format("{0}/HelpPages/{1}.html", curDir, key); MessageBox.Show("Key " + key);
            if (!File.Exists(path))
            {
                key = "Map";    // Should be error
            }
            Uri u = new Uri(String.Format("file:///{0}/HelpPages/{1}.html", curDir, key));
            MessageBox.Show("Uri " + u);
            ch = new JavaScriptControlHelper(originator);
            wbHelp.ObjectForScripting = ch;
            wbHelp.Navigate(u);

        }

        /// <summary>
        ///     This HelpViewer is opened by <c>Species Dialog</c> window calling.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originator"></param>
        public HelpViewer(string key, SpeciesDialog originator)
        {
            InitializeComponent();
            string curDir = Directory.GetCurrentDirectory();
            string path = String.Format("{0}/HelpPages/{1}.html", curDir, key); MessageBox.Show("Key " + key);
            if (!File.Exists(path))
            {
                key = "Species/AddSpecies"; // Should be error
            }
            Uri u = new Uri(String.Format("file:///{0}/HelpPages/{1}.html", curDir, key));
            MessageBox.Show("Uri " + u);
            ch = new JavaScriptControlHelper(originator);
            wbHelp.ObjectForScripting = ch;
            wbHelp.Navigate(u);

        }

        private void BrowseBack_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ((wbHelp != null) && (wbHelp.CanGoBack));
        }

        private void BrowseBack_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            wbHelp.GoBack();
        }

        private void BrowseForward_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ((wbHelp != null) && (wbHelp.CanGoForward));
        }

        private void BrowseForward_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            wbHelp.GoForward();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void wbHelp_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }


    }
}
